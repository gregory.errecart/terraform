# Création des WareHouse
resource "snowflake_warehouse" "ingestion_warehouse" {
  name           = "INGESTION_WAREHOUSE"
  warehouse_size = "X-SMALL"
  auto_suspend   = 300
  auto_resume    = true
}

resource "snowflake_warehouse" "traitement_warehouse" {
  name           = "TRAITEMENT_WAREHOUSE"
  warehouse_size = "X-SMALL"
  auto_suspend   = 300
  auto_resume    = true
}

# Création des Users
resource "snowflake_user" "ingestion_user" {
  name     = "INGESTION_USER"
  password = var.snowflake_password_account_ingestion
}

resource "snowflake_user" "traitement_user" {
  name     = "TRAITEMENT_USER"
  password = var.snowflake_password_account_traitement
}

# Création des rôles
resource "snowflake_role" "ingestion_role" {
  name = "INGESTION_ROLE"
}

resource "snowflake_role" "traitement_role" {
  name = "TRAITEMENT_ROLE"
}

# Attribution des rôles aux users
resource "snowflake_role_grants" "ingestion_user_role_grants" {
  role_name = snowflake_role.ingestion_role.name

  users = [
    snowflake_user.ingestion_user.name,
  ]
}

resource "snowflake_role_grants" "traitement_user_role_grants" {
  role_name = snowflake_role.traitement_role.name

  users = [
    snowflake_user.traitement_user.name,
  ]
}

# Attribution des droits
resource "snowflake_warehouse_grant" "ingestion_warehouse_grant" {
  warehouse_name = snowflake_warehouse.ingestion_warehouse.name
  privilege      = "USAGE"
  roles          = [snowflake_role.ingestion_role.name]
}

resource "snowflake_warehouse_grant" "traitement_warehouse_grant" {
  warehouse_name = snowflake_warehouse.traitement_warehouse.name
  privilege      = "USAGE"
  roles          = [snowflake_role.traitement_role.name]
}

# Création des DB
resource "snowflake_database" "bronze" {
  name = "BRONZE"
}

resource "snowflake_database" "silver" {
  name = "SILVER"
}

# Création des schémas
resource "snowflake_schema" "airbyte_schema" {
  database = snowflake_database.bronze.name
  name     = "AIRBYTE"
}

resource "snowflake_schema" "dbt_schema" {
  database = snowflake_database.silver.name
  name     = "DBT"
}

# Application des permissions
resource "snowflake_database_grant" "bronze_grant" {
  database_name = snowflake_database.bronze.name
  privilege     = "ALL PRIVILEGES"
  roles         = [snowflake_role.ingestion_role.name]
}

resource "snowflake_database_grant" "silver_grant" {
  database_name = snowflake_database.silver.name
  privilege     = "ALL PRIVILEGES"
  roles         = [snowflake_role.traitement_role.name]
}

resource "snowflake_schema_grant" "airbyte_schema_grant" {
  schema_name   = snowflake_schema.airbyte_schema.name
  database_name = snowflake_database.bronze.name
  privilege     = "ALL PRIVILEGES"
  roles         = [snowflake_role.ingestion_role.name]
}

resource "snowflake_schema_grant" "dbt_schema_grant" {
  schema_name   = snowflake_schema.dbt_schema.name
  database_name = snowflake_database.silver.name
  privilege     = "ALL PRIVILEGES"
  roles         = [snowflake_role.traitement_role.name]
}


# Accorder le droit de faire des tables
# resource "snowflake_table_grant" "airbyte_future_tables_allprivileges_grant" {
#   database_name = snowflake_database.bronze.name
#   schema_name   = snowflake_schema.airbyte_schema.name
#   roles         = [snowflake_role.ingestion_role.name]
#   privilege     = "ALL PRIVILEGES"
#   on_future     = true
#   with_grant_option = true
# }

# resource "snowflake_table_grant" "airbyte_future_tables_create_grant" {
#   database_name = snowflake_database.bronze.name
#   schema_name   = snowflake_schema.airbyte_schema.name
#   roles         = [snowflake_role.ingestion_role.name]
#   privilege     = "CREATE"
#   on_future     = true
#   with_grant_option = true
# }

# resource "snowflake_table_grant" "airbyte_future_tables_insert_grant" {
#   database_name = snowflake_database.bronze.name
#   schema_name   = snowflake_schema.airbyte_schema.name
#   roles         = [snowflake_role.ingestion_role.name]
#   privilege     = "INSERT"
#   on_future     = true
#   with_grant_option = true
# }

# resource "snowflake_table_grant" "airbyte_future_tables_update_grant" {
#   database_name = snowflake_database.bronze.name
#   schema_name   = snowflake_schema.airbyte_schema.name
#   roles         = [snowflake_role.ingestion_role.name]
#   privilege     = "UPDATE"
#   on_future     = true
#   with_grant_option = true
# }

# resource "snowflake_table_grant" "airbyte_future_tables_delete_grant" {
#   database_name = snowflake_database.bronze.name
#   schema_name   = snowflake_schema.airbyte_schema.name
#   roles         = [snowflake_role.ingestion_role.name]
#   privilege     = "DELETE"
#   on_future     = true
#   with_grant_option = true
# }

# Faire le airbyte terraform
resource "airbyte_source_faker" "my_source_faker" {
  configuration = {
    sourceType = "faker"
  }
  name         = "faker"
  workspace_id = "26a9c1ac-087a-4c39-87fb-fc1ac702c6d4"
}

resource "airbyte_destination_snowflake" "my_destination_snowflake" {
  name = "snowflake"
  configuration = {
    credentials = {
      username_and_password = {
        password = var.snowflake_password_account_ingestion
      }
    }
    database = "BRONZE"
    host = "zohmswl-go32061.snowflakecomputing.com"
    role = "INGESTION_ROLE"
    schema = "AIRBYTE"
    username = "INGESTION_USER"
    warehouse = "INGESTION_WAREHOUSE"
  }
  workspace_id = "26a9c1ac-087a-4c39-87fb-fc1ac702c6d4"
}

resource "airbyte_connection" "faker_snowflake" {
  name           = "Faker to Snowflake"
  source_id      = airbyte_source_faker.my_source_faker.source_id
  destination_id = airbyte_destination_snowflake.my_destination_snowflake.destination_id
}
