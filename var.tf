variable "snowflake_account" {
    description = "Snowflake account"
    type = string
    sensitive = true
}
variable "snowflake_username" {
    description = "Snowflake username"
    type = string
    sensitive = true
}
variable "snowflake_password" {
    description = "Snowflake password"
    type = string
    sensitive = true
}

variable "snowflake_password_account_ingestion" {
    description = "Snowflake password"
    type = string
    sensitive = true
}

variable "snowflake_password_account_traitement" {
    description = "Snowflake password"
    type = string
    sensitive = true
}